﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float speed;

    public float avanzar = 0;
    public float morir = 50;
    // Update is called once per frame
    void Update()
    {

            transform.Translate(-speed * Time.deltaTime, 0, 0);
            avanzar++;
            if(avanzar>= morir){
            Destroy(gameObject);
            }

    }

}
