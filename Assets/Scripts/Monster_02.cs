﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_02 : MonoBehaviour
{
    public Vector2 speed;

    // Start is called before the first frame update


    void Update(){
        transform.Translate(speed*Time.deltaTime);
               
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "EnemieChangeSpeed") {
            speed.x= speed.x*-1;
            }
    }           
 


}