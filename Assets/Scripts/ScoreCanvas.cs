﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class ScoreCanvas : MonoBehaviour
{
    [SerializeField] Text gems;

    private int gemCount;
 
    // Start is called before the first frame update
    void Start()
    {
        gemCount = 0;
        gems.text = gemCount.ToString("0");
    }

    public void AddGems(int value){
        gemCount+=value;
        gems.text = gemCount.ToString("0");
    }

}