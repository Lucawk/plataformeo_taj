﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    // Start is called before the first frame update
    public void PulsaPlay()
    {
        SceneManager.LoadScene("Gameplay");
    }

    // Update is called once per frame
    public void PulsaExit()
    {
        Application.Quit();
    }

    public void PulsaCredits()
    {
        SceneManager.LoadScene("Credits");
    }

        public void PulsaMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    
 /*        public void PulsaControles()
    {
        SceneManager.LoadScene("Controls");
    }
    */
}
