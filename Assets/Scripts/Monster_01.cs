﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_01 : MonoBehaviour
{
    // Start is called before the first frame update

    Vector2 speed;


    Transform graphics;
    public float TimeToShoot = 0;
    public float TimeToJump = 0;
    public float Shoot = 30;
    public float Jump = 50;
    public float stopjump=60;

    public GameObject EnemyBullet;

    void Awake()
    {

        speed.y = 5;
    }

   
    void Update()
    {
        TimeToJump++;
        TimeToShoot++;

        if(TimeToJump >= Jump){

            transform.Translate(speed*Time.deltaTime);

            if(TimeToJump >= stopjump){
                TimeToJump = 0;
                speed.y *= -1;
            }
        }

        if(TimeToShoot >= Shoot){
            
        Instantiate (EnemyBullet, this.transform.position, Quaternion.identity, null);
        TimeToShoot = 0;

        }


    }
}
